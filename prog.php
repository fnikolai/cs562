<?php 
set_time_limit(0);
// Run jar file
chdir('./');


// Connect to DB
$host="localhost"; // Host name
$user="root"; // Mysql username
$pass="dummypass" ; // Mysql password
$db_name="cs562"; // Database name
$tbl_name = "flickrGrowth";

$link=mysql_connect("$host", "$user", "$pass")or die("<center><b><br><br>cannot connect");
mysql_select_db("$db_name")or die("cannot select DB");

$filename = 'sqlQuery.txt';
if (file_exists($filename))
    file_put_contents($filename, "IDLE FROM THE BEGGINING");
$user = 1;
$SPARQL_PREFIX = ' prefix cs562:<http://www.semanticweb.org/kondylak/ontologies/2014/4/untitled-ontology-16#> ';


function readQueryFile($filename) 
{
    $sqlQuery = "";
    $sqlQuery = file_get_contents( $filename );
        /*
        $loop = 0;
        while(empty($sqlQuery) )
        {
            $loop = $loop + 1;
            if ($loop > 3 ) {
                echo "Wait for a looooong time";
                exec('java -jar app.jar "'.$sql.'"');
                sleep(10);
                $sqlQuery = file_get_contents( $filename );
            }
            echo "<br> SqlQuert is empty";
            $sqlQuery = file_get_contents( $filename );
            sleep(2);
        }
        return $sqlQuery;
         */
    if (empty($sqlQuery)){
        echo "Trying query : " . $sql . "\n";
        die( "cannot read contents of " . $sqlQuery);
    }


    file_put_contents($filename, "IDLE");
    return $sqlQuery;
}


?>
<form action='prog.php' method='post' >
<table  style="width:80%" >

<caption><b>User-Centric Queries for Friends</b></caption>
<tr>
<td>
<b>
Select User: 
 </b>
</td>
<td>
<select  name='user'>
<?php
$sql="SELECT DISTINCT User1 FROM $tbl_name";
$result=mysql_query($sql);
while($info = mysql_fetch_array($result)){
    echo "<option value='".$info[0]."'>".$info[0]."</option>";
}
?>
</select> 
</td>
</tr>
<td>
<b>Select Predicates: </b>
</td>
<td>
Number of views
<select name='csViews'>
<option value='gt'> >= </option>
<option value='lt'> <= </option>
<option value='eq'> = </option>
</select>
<input type = "text" name = "views" id = "views" maxlength="5" style ="width:40px">
</td>
<td>
Number of comments
<select name='csComments'>
<option value='gt'> >= </option>
<option value='lt'> <= </option>
<option value='eq'> = </option>
</select>
<input type = "text" name = "comments" maxlength="5" style ="width:40px">
</td>
<td>
Number of Fav Markings
<select name='csFav'>
<option value='gt'> >= </option>
<option value='lt'> <= </option>
<option value='eq'> = </option>
</select>
<input type = "text" name = "markings" maxlength="5" style ="width:40px">
</td>

</tr>
<tr>
<td>
<b> Select Period </b>
</td>
<td  align=center>
From :
<input type='text' size='4' name='from1' maxlength=4>&nbsp -
<input type='text' size='2' name='from2' maxlength=2>&nbsp -
<input type='text' size='2' name='from3' maxlength=2>
</td>

<td  align=center>
To :
<input type='text' size='4' name='to1' maxlength=4>&nbsp -
<input type='text' size='2' name='to2' maxlength=2>&nbsp -
<input type='text' size='2' name='to3' maxlength=2>
</td>

</tr>

<tr>
<td>
<b>Use Ranking:</b>
</td>
<td>
<input type = "checkbox">
</td>
</tr>
</table>
<center>
<input type='submit' name='UCF' class='button' value='Run'>
</center>
</form>
<?php
if (isset($_POST['UCF'])){
    $user = $_POST['user'];

    $numViews = $_POST['views'];
    $csViews = $_POST['csViews'];

    $numComments = $_POST['comments'];
    $csComments = $_POST['csComments'];

    $numMarkings = $_POST['markings'];
    $csFav = $_POST['csFav'];

    $from1=$_POST['from1'];
    $from2=$_POST['from2'];
    $from3=$_POST['from3'];

    $to1=$_POST['to1'];
    $to2=$_POST['to2'];
    $to3=$_POST['to3'];	


    $from = $from1."-".$from2."-".$from3;
    $to = $to1."-".$to2."-".$to3;
        /*echo "FROM : ".$from."<br>";
        echo "TO : ".$to."<br>";*/

    if($numViews == "")
        $numViews  = 0;
    if($numComments == "")
        $numComments  = 0;
    if($numMarkings == "")
        $numMarkings  = 0;

    // Retrieve friends of user
    $sparql = $SPARQL_PREFIX . ' select distinct ?user where { { ?fr cs562:friendship_from_user ?user; cs562:friendship_to_user cs562:user_'.$user.'} union { ?fr cs562:friendship_from_user cs562:user_'.$user.' ; cs562:friendship_to_user ?user} }';


    /* Reset text file */
    $sqlQuery = "";
    file_put_contents($filename, "WAITING FOR USERS");
    /* Format it for input to .jar */
    $sparql = '" ' . $sparql . '"';
    // echo "<br> SPARQL  for getting User's friends : $sparql <br>"; // Print sparql query
    exec("java -jar app.jar " . $sparql);

    $sqlQuery = readQueryFile($filename);
    $result=mysql_query($sqlQuery);

    //   echo "Resulting SQL query for friends : " . $sqlQuery;
//    echo "Friends of ".$user."<br>";
    $users[] = "";
    $i = 0;
    while($info = mysql_fetch_array($result)){
  //      echo $info[2]." <br> ";
        $users[$i] = strstr($info[2],'user');
        $i++;
    }
    echo "Friends of User  : ". $user . "<br>"  . print_r(array_values($users));


    //   echo "Number of Users : " . sizeof($users); // print number of users

    $pred = "";
    // Retrieve photos with Predicates

    if($csViews == 'gt')
        $pred .= "?Photos cs562:views_number ?views. filter(?views >= $numViews) ";
    elseif($csViews == 'lt')
        $pred .= "?Photos cs562:views_number ?views. filter(?views <= $numViews) }";
    else
        $pred .= "?Photos cs562:views_number ?views. filter(?views = $numViews) }";

    if($csComments == 'gt')
        $pred .= ". ?Photos cs562:comments_number ?comments. filter(?comments >= $numComments) ";
    elseif($csComments == 'lt')
        $pred .= ". ?Photos cs562:comments_number ?comments. filter(?comments <= $numComments) ";
    else
        $pred .= ". ?Photos cs562:comments_number ?comments. filter(?comments = $numComments)";

    if($csFav == 'gt')
        $pred .= ". ?Photos cs562:favorite_markings_number  ?numMark. filter(?numMark >= $numMarkings) ";
    elseif($csFav == 'lt')
        $pred .= ". ?Photos cs562:favorite_markings_number  ?numMark. filter(?numMark <= $numMarkings) ";
    else
        $pred .= ". ?Photos cs562:favorite_markings_number  ?numMark. filter(?numMark = $numMarkings)";

    $sql .= $pred;

    // Reset text file 
    file_put_contents($filename, "WAITING FOR PHOTOS");
    $sqlQueryPhotos = "";
    $j = 0;
    $before = microtime(true);

    $spar = $SPARQL_PREFIX . " select ?Photos ?Users  where {{?Favorite_Marking cs562:favorite_marking_to_photo ?Photos. ?Favorite_Marking cs562:favorite_marking_from_user ?Users. ?Favorite_Marking cs562:favorite_marking_time ?time.} UNION{?Photos cs562:belongs_to ?Users. ?Photos cs562:upload_time ?time. }" . $pred . " }";


    /* Reset text file */
    $sqlQuery = "";
    file_put_contents($filename, "WAITING FOR SOMETHING");
    /* Format it for input to .jar */
    $spar = '" '. $spar . '"';
    //echo "<br> SPARQL  for getting Users liking photos : $spar <br>"; // Print sparql query
    exec('java -jar app.jar ' . $spar);

    $sqlQuery = readQueryFile($filename);
    $result = mysql_query($sqlQuery);
    $num_rows = mysql_num_rows($result);
    echo " Num of rows : " . $num_rows . "<br>";
    if($num_rows > 0) 
    {
        while($info = mysql_fetch_array($result)){
            $resultsUsers[$j] = strstr( $info[5],'user');
            $j++;
        }
    }

    file_put_contents($filename, "IDLE");
    $after = microtime(true);
    echo ($after-$before)." <br>";
    echo sizeof($resultsUsers);
    echo "Results Users : " . sizeof($resultsUsers) . " <br>";
    echo "Users : " . print_r(array_values($resultsUsers)) . "<br";

    $friendsToPhotos = array_intersect($resultsUsers, $users);
    echo "FriendsToPhotos : " . array_values($resultsUsers) . "<br>";
}       

echo "<br> Check (user, photo) edge <br>";



?>
<br>

<form action='prog.php' method='post' >
<table  style="width:80%" >

<caption><b>User-Centric Queries for Objects</b></caption>
<tr>
<td>
<b>
Select User: 
 </b>
</td>
<td>
<select  name='user'>
<?php
$sql="SELECT DISTINCT User1 FROM $tbl_name";
$result=mysql_query($sql);
while($info = mysql_fetch_array($result)){
    echo "<option value='".$info[0]."'>".$info[0]."</option>";
}
?>
</select> 
</td>
</tr>
<tr>
<td>
<b>Select Predicates: </b>
</td>
<td>
Number of photos owned
<select name='csOwned'>
<option value='gt'> >= </option>
<option value='lt'> <= </option>
<option value='eq'> = </option>
</select>
<input type = "text" name = "numOwned" maxlength="5" style ="width:40px">
</td>
<td>
Number of Fav Markings
<select>
<option> >= </option>
<option> <= </option>
<option> = </option>
</select>
<input type = "text" name = "markings" maxlength="5" style ="width:40px">
</td>
</tr>
<tr>
<td>
<b> Select Period </b>
</td>
<td>

</td>
</tr>
<tr>
<td>
<b>Use Ranking:</b>
</td>
<td>
<input type = "checkbox">
</td>
</tr>
</table>
<center>
<input type='submit' name='UCO' class='button' value='Run'>
</center>
</form>

<?php
if (isset($_POST['UCO'])){
    $user = $_POST['user'];

    $numOwned = $_POST['numOwned'];
    $csOwned = $_POST['csOwned'];

    if($numOwned == "")
        $numOwned  = 0;

    // Retrieve friends of user
    $sparql = $SPARQL_PREFIX . ' select distinct ?user where { { ?fr cs562:friendship_from_user ?user; cs562:friendship_to_user cs562:user_'.$user.'} union { ?fr cs562:friendship_from_user cs562:user_'.$user.' ; cs562:friendship_to_user ?user} }';


    /* Reset text file */
    $sqlQuery = "";
    file_put_contents($filename, "WAITING FOR USERS");
    /* Format it for input to .jar */
    $sparql = '" ' . $sparql . '"';
    exec("java -jar app.jar " . $sparql);
    $sqlQuery = readQueryFile($filename);
    $result=mysql_query($sqlQuery);

    $userFriends[] = "";
    $i = 0;
    while($info = mysql_fetch_array($result)){
        $userFriends[$i] = strstr($info[2],'user');
        $i++;
    }

    // Retrieve friends of user
    /*$sparql = $SPARQL_PREFIX . ' select ?User (count(distinct ?user) as ?total) where { ?photo cs562:belongs_to ?User } ';

    $sqlQuery = "";
    file_put_contents($filename, "WAITING FOR USERS");
    $sparql = '" ' . $sparql . '"';
         echo "<br> SPARQL  for getting User's friends : $sparql <br>"; // Print sparql query
    exec("java -jar app.jar " . $sparql);

    $sqlQuery = readQueryFile($filename);*/
    $sqlQuery = "SELECT DISTINCT PhotoOwner, COUNT(*) AS total FROM flickrallphotos GROUP BY PhotoOwner HAVING " ;

    if($csOwned == 'gt')
        $sqlQuery .= "total >= $numOwned";
    elseif($csOwned == 'lt')
        $sqlQuery .= "total <= $numOwned";
    else
        $sqlQuery .= "total = $numOwned";

    $sqlQuery .= " ORDER BY total";


    $result=mysql_query($sqlQuery);

    $users[] = "";
    $i = 0;
    while($info = mysql_fetch_array($result)){
        $user = array("user_".$info[0]);

        $resultUser = array_intersect($user, $userFriends);
        if(sizeof($resultUser) > 0){
            $users[$i] = $info[0];
            $i++;
        }
    }
    //echo $users[0];

    // RETRIVE res(Q)
    $j = 0;
    $sql = "SELECT distinct * FROM flickrfavmarks";
    $result=mysql_query($sql);
    $before = microtime(true);
    while($info = mysql_fetch_array($result)){

        // Take User
        $user = array($info[0]);
        $resultUser = array_intersect($user, $users);

        if(sizeof($resultUser) > 0){
            if(isset($resultsObjects[$info[1]])){
                $resultsObjects[$info[1]]++;
                $j++;
            }
            else{
                $resultsObjects[$info[1]] = 0;
                $resultsObjects[$info[1]]++;
            }
        }
    }
    $after = microtime(true);
    echo "<br> TIME: ".($after-$before)." <br>";
    echo "res(Q) : ";
    foreach($resultsObjects as $k => $v)
        if(sizeof($userFriends) == $v)
            echo $k. " ";
}	

?>

<form action='prog.php' method='post' >
<table  style="width:80%" >
        <caption><b>System-Centric Queries for Users</b></caption>
        <tr>
                <td>
                        <b>Select Predicates: </b>
                </td>
                <td>
                        Number of views
                        <select name='csViews'>
                                <option value='gt'> >= </option>
                                <option value='lt'> <= </option>
                                <option value='eq'> = </option>
                        </select>
                        <input type = "text" name = "views" id = "views" maxlength="5" style ="width:40px">
                </td>
                <td>
                        Number of comments
                        <select name='csComments'>
                                <option value='gt'> >= </option>
                                <option value='lt'> <= </option>
                                <option value='eq'> = </option>
                        </select>
                        <input type = "text" name = "comments" maxlength="5" style ="width:40px">
                </td>
                <td>
                        Number of Fav Markings
                        <select name='csFav'>
                                <option value='gt'> >= </option>
                                <option value='lt'> <= </option>
                                <option value='eq'> = </option>
                        </select>
                        <input type = "text" name = "markings" maxlength="5" style ="width:40px">
                </td>

        </tr>
<tr>
<td>
<b> Select Period </b>
</td>
<td>

</td>
</tr>

<tr>
<td>
<b>Use Ranking:</b>
</td>
<td>
<input type = "checkbox">
</td>
</tr>
</table>
<center>
<input type='submit' name='SCF' class='button' value='Run'>
</center>
</form>

<?php
if (isset($_POST['SCF'])){

    $numViews = $_POST['views'];
    $csViews = $_POST['csViews'];

    $numComments = $_POST['comments'];
    $csComments = $_POST['csComments'];

    $numMarkings = $_POST['markings'];
    $csFav = $_POST['csFav'];

    if($numViews == "")
        $numViews  = 0;
    if($numComments == "")
        $numComments  = 0;
    if($numMarkings == "")
        $numMarkings  = 0;


    $pred = "";
    // Retrive photos with Predicates
    $sql = $SPARQL_PREFIX . ' select distinct ?photo where{   ';

    if($csViews == 'gt')
        $pred .= "?photo cs562:views_number ?views. filter(?views >= $numViews) ";
    elseif($csViews == 'lt')
        $pred .= "?photo cs562:views_number ?views. filter(?views <= $numViews) }";
    else
        $pred .= "?photo cs562:views_number ?views. filter(?views = $numViews) }";

    if($csComments == 'gt')
        $pred .= ". ?photo cs562:comments_number ?comments. filter(?comments >= $numComments) ";
    elseif($csComments == 'lt')
        $pred .= ". ?photo cs562:comments_number ?comments. filter(?comments <= $numComments) ";
    else
        $pred .= ". ?photo cs562:comments_number ?comments. filter(?comments = $numComments)";

    if($csFav == 'gt')
        $pred .= ". ?photo cs562:favorite_markings_number  ?numMark. filter(?numMark >= $numMarkings) ";
    elseif($csFav == 'lt')
        $pred .= ". ?photo cs562:favorite_markings_number  ?numMark. filter(?numMark <= $numMarkings) ";
    else
        $pred .= ". ?photo cs562:favorite_markings_number  ?numMark. filter(?numMark = $numMarkings)";

    $sql .= $pred;
    $sql .= " } ";


    // Reset text file 
    file_put_contents($filename, "WAITING FOR PHOTOS");
    $sqlQueryPhotos = "";
    // Format it for input to .jar
    $sql = '" ' . $sql . '"';
    // echo "<br> SPARQL for fetching photos : $sql <br>"; // Print sparql query
    exec("java -jar app.jar " . $sql);

    $sqlQueryPhotos = readQueryFile($filename);
    //echo "<br> SQL for fetching photos : $sqlQuery <br>"; // Print sparql query

    $result=mysql_query($sqlQueryPhotos);

    // Photos found given predicates
    $i = 0;

    echo "Photos retrieved with selected predicates <br>" ;
    while($info = mysql_fetch_array($result)){
        //echo $info[2]." <br> ";
        $photos[$i] = strstr($info[2], 'photo');
        $i++;
    }

    echo "<br>Size of photos : ".sizeof($photos)." <br>";
    echo "<br> Check (user, photo) edge <br>";
    $j = 0;
    //$resultsUsers[] = 0;

    $sql = "SELECT * FROM flickrfavmarks";
    $result=mysql_query($sql);
    $before = microtime(true);
    while($info = mysql_fetch_array($result)){
        // Take Photo
        $photo = array("photo_".$info[1]);
        $resultPhoto = array_intersect($photo, $photos);
        if(sizeof($resultPhoto) > 0){
            $resultsUsers[$j] = $info[0];
            $j++;
        }
    }
    $after = microtime(true);
    echo "<br> TIME: ".($after-$before)." <br>";
    echo "res(Q) : ";
    foreach($resultsUsers as $k => $v)
        echo $v. " ";

}       
?>


<form action='prog.php' method='post' >
<table  style="width:80%" >

<caption><b>System-Centric Queries for Objects</b></caption>
<tr>
<td>
<b>Select Predicates: </b>
</td>
<td>
Number of photos owned
<select name='csOwned'>
<option value='gt'> >= </option>
<option value='lt'> <= </option>
<option value='eq'> = </option>
</select>
<input type = "text" name = "numOwned" maxlength="5" style ="width:40px">
</td>
</tr>
<tr>
<td>
<b> Select Period </b>
</td>
<td>

</td>
</tr>
<tr>
<td>
<b>Use Ranking:</b>
</td>
<td>
<input type = "checkbox">
</td>
</tr>
</table>
<center>
<input type='submit' name='SCO' class='button' value='Run'>
</center>
</form>

<?php
if (isset($_POST['SCO'])){


    $numOwned = $_POST['numOwned'];
    $csOwned = $_POST['csOwned'];

    if($numOwned == "")
        $numOwned  = 0;

    // Retrieve friends of user
    /*$sparql = $SPARQL_PREFIX . ' select ?User (count(distinct ?user) as ?total) where { ?photo cs562:belongs_to ?User } ';

    $sqlQuery = "";
    file_put_contents($filename, "WAITING FOR USERS");
    $sparql = '" ' . $sparql . '"';
         echo "<br> SPARQL  for getting User's friends : $sparql <br>"; // Print sparql query
    exec("java -jar app.jar " . $sparql);

    $sqlQuery = readQueryFile($filename);*/
    $sqlQuery = "SELECT DISTINCT PhotoOwner, COUNT(*) AS total FROM flickrallphotos GROUP BY PhotoOwner HAVING " ;

    if($csOwned == 'gt')
        $sqlQuery .= "total >= $numOwned";
    elseif($csOwned == 'lt')
        $sqlQuery .= "total <= $numOwned";
    else
        $sqlQuery .= "total = $numOwned";

    $sqlQuery .= " ORDER BY total";


    $result=mysql_query($sqlQuery);

    $users[] = "";
    $i = 0;
    while($info = mysql_fetch_array($result)){
        //echo $info[0]." ".$info[1]." <br> ";
        //$users[$i] = strstr($info[2],'user');
        $users[$i] = $info[0];
        $i++;
    }


    // RETRIVE res(Q)
    $j = 0;
    $sql = "SELECT distinct * FROM flickrfavmarks";
    $result=mysql_query($sql);
    $before = microtime(true);
    while($info = mysql_fetch_array($result)){

        // Take User
        $user = array($info[0]);
        $resultUser = array_intersect($user, $users);

        if(sizeof($resultUser) > 0){
            if(isset($resultsObjects[$info[1]])){
                $resultsObjects[$info[1]]++;
                $j++;
            }
            else{
                $resultsObjects[$info[1]] = 0;
                $resultsObjects[$info[1]]++;
            }
        }
    }
    $after = microtime(true);
    echo "<br> TIME: ".($after-$before)." <br>";
    echo "res(Q) : ";
    foreach($resultsObjects as $k => $v)
        if(sizeof($users) == $v)
            echo $k. " ";

}
?>
