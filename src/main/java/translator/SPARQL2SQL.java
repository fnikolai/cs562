/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package translator;



/**
 *
 * @author manolis
 */
import it.unibz.krdb.obda.io.ModelIOManager;
import it.unibz.krdb.obda.model.OBDADataFactory;
import it.unibz.krdb.obda.model.OBDAModel;
import it.unibz.krdb.obda.model.impl.OBDADataFactoryImpl;
import it.unibz.krdb.obda.owlrefplatform.core.QuestConstants;
import it.unibz.krdb.obda.owlrefplatform.core.QuestPreferences;
import it.unibz.krdb.obda.owlrefplatform.owlapi3.QuestOWL;
import it.unibz.krdb.obda.owlrefplatform.owlapi3.QuestOWLConnection;
import it.unibz.krdb.obda.owlrefplatform.owlapi3.QuestOWLFactory;
import it.unibz.krdb.obda.owlrefplatform.owlapi3.QuestOWLResultSet;
import it.unibz.krdb.obda.owlrefplatform.owlapi3.QuestOWLStatement;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLObject;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.reasoner.SimpleConfiguration;




public class SPARQL2SQL {

	/*
	 * Use the sample database using H2 from
	 * https://github.com/ontop/ontop/wiki/InstallingTutorialDatabases
	 * 
	 * Please use the pre-bundled H2 server from the above link
	 * 
	 */
	final String owlfile = "./configurations/Flickr.owl";
	final String obdafile = "./configurations/Flickr.obda";

	public void runQuery(String sparqlQuery) throws Exception {

		/*
		 * Load the ontology from an external .owl file.
		 */
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		OWLOntology ontology = manager.loadOntologyFromOntologyDocument(new File(owlfile));
		
		/*
		 * Load the OBDA model from an external .obda file
		 */
		OBDADataFactory fac = OBDADataFactoryImpl.getInstance();
		OBDAModel obdaModel = fac.getOBDAModel();
		ModelIOManager ioManager = new ModelIOManager(obdaModel);
		ioManager.load(new File(obdafile));

		
		/*
		 * Prepare the configuration for the Quest instance. The example below shows the setup for
		 * "Virtual ABox" mode
		 */
		QuestPreferences preference = new QuestPreferences();
		preference.setCurrentValueOf(QuestPreferences.ABOX_MODE, QuestConstants.VIRTUAL);

		/*
		 * Create the instance of Quest OWL reasoner.
		 */
		QuestOWLFactory factory = new QuestOWLFactory();
		factory.setOBDAController(obdaModel);
		factory.setPreferenceHolder(preference);
		QuestOWL reasoner = (QuestOWL) factory.createReasoner(ontology, new SimpleConfiguration());

		/*
		 * Prepare the data connection for querying.
		 */
		
		
		QuestOWLConnection conn = reasoner.getConnection();
		QuestOWLStatement st = conn.createStatement();

		/*
		 * Get the book information that is stored in the database
		 */
		
		sparqlQuery = 
				 "PREFIX : <http://www.semanticweb.org/kondylak/ontologies/2014/4/untitled-ontology-16#>" +
                         "select ?Users\n" +
                         " where { \n" +
                         "{?friendship :friendship_from_user ?User1. \n"+
                         " ?friendship :friendship_to_user ?Users. \n"+
                         "FILTER(?User=\"1\")}"+
                         "UNION{ ?friendship :friendship_from_user ?Users. \n"+
                         "?friendship :friendship_to_user ?User2. \n"+
                         "FILTER(?User=\"1\")}"+ 
                          "} LIMIT 2";
				
				/*
				"PREFIX : <http://www.semanticweb.org/kondylak/ontologies/2014/4/untitled-ontology-16#> \n" +
				"SELECT ?y \n" +
				"WHERE { ?y a :Favorite_Marking."
                        + " ?y :favorite_marking_to_photo :photo_14539179 }";
				*/
		try {
			//System.out.println("Skip SQL  execution");
			
			/* This is the code for fetching rows from the DB */
		
			QuestOWLResultSet rs = st.executeTuple(sparqlQuery);
			int columnSize = rs.getColumCount();
			while (rs.nextRow()) {
				for (int idx = 1; idx <= columnSize; idx++) {
					OWLObject binding = rs.getOWLObject(idx);
					System.out.print(binding.toString() + ", ");
				}
				System.out.print("\n");
			
			
			}
			//rs.close();

			/*
			 * Print the query summary
			 */
			QuestOWLStatement qst = (QuestOWLStatement) st;
			String sqlQuery = qst.getUnfolding(sparqlQuery);

			System.out.println();
			System.out.println("The input SPARQL query:");
			System.out.println("=======================");
			System.out.println(sparqlQuery);
			System.out.println();

			System.out.println("The output SQL query:");
			System.out.println("=====================");
			System.out.println(sqlQuery);

			System.out.println("\nPreparing and writing to sqlQuery.txt");
			System.out.println("=====================");

			PrintWriter writer = new PrintWriter("sqlQuery.txt", "UTF-8");
			writer.println(sqlQuery + ";");
			writer.close();

		} finally {

			/*
			 * Close connection and resources
			 */
			if (st != null && !st.isClosed()) {
				st.close();
			}
			if (conn != null && !conn.isClosed()) {
				conn.close();
			}
			reasoner.dispose();
		}
	}

	/**
	 * Main client program
	 */
	public static void main(String[] args) {
		try {
			
			if (args.length == 0 )
			{
				System.out.println("No SPARQL  query provided");
				System.exit(-1);
			}
			
			//System.out.println("Query: " + args[0]); 
			
			String sparql = args[0];
			SPARQL2SQL example = new SPARQL2SQL();
			example.runQuery(sparql);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

